<?php
$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `course`";
// var_dump($query);
include 'header.php'; 
?>

<div class="container" style="margin-top: 30px;">  
     <a href="index.php" class="btn btn-success btn-lg">Add New</a>       
  <table class="table table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>Subject Code</th>
        <th>Subject Title</th>
        <th>Department</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
         <?php 
        foreach($db->query($query) as $course) { ?>
        <tr>
          <td><?php echo $course['id'];?></td>
          <td><?php echo $course['subject_code'];?></td>
          <td><?php echo $course['subject_title'];?></td>
          <td><?php echo $course['department'];?></td>
          <td>
            <a href="view.php?id=<?php echo $course['id'];?>" class="btn btn-primary">Show</a>
            <a href="edit.php?id=<?php echo $course['id'];?>" class="btn btn-warning">Edit</a>
            <a href="delete.php?id=<?php echo $course['id'];?>" class="btn btn-danger">Delete</a>
          </td>
        </tr>
      <?php }
      ?>
    </tbody>
  </table>
</div>

<?php include 'footer.php'; ?>