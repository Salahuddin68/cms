<?php
$db = new PDO('mysql:host=localhost;dbname=crud;charset=utf8mb4', 'root', '');
$query = "SELECT * FROM `course` WHERE id =".$_GET['id'];
// var_dump($query);
include 'header.php'; 
?>

<div class="container" style="margin-top: 30px;">  
  <a href="index.php" class="btn btn-primary btn-lg">Add New</a>       
  <a href="viewlist.php" class="btn btn-primary btn-lg">View All</a>       
  <table class="table table-hover">
    <thead>
      <tr>
        <th>ID</th>
        <th>Subject Code</th>
        <th>Subject Title</th>
        <th>Department</th>
      </tr>
    </thead>
    <tbody>
         <?php 
        foreach($db->query($query) as $course) { ?>
        <tr>
          <td><?php echo $course['id'];?></td>
          <td><?php echo $course['subject_code'];?></td>
          <td><?php echo $course['subject_title'];?></td>
          <td><?php echo $course['department'];?></td>
        </tr>
      <?php }
      ?>
    </tbody>
  </table>
</div>

<?php include 'footer.php'; ?>