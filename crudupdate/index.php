<?php include 'header.php'; ?>

<div class="container" style="margin-top: 50px;">
  <div class="row">
      <div class="col-md-12">
          <fieldset>
              <legend>course Information:</legend>
             <form action="store.php" method="POST" enctype="multipart/form-data">
                 <div class="form-group">
                     <label>Subject Code:</label>
                     <input type="text" class="form-control" name="sub_code"  placeholder="Subject Code">
                 </div>
                 <div class="form-group">
                     <label>Subject Title:</label>
                     <input type="text" class="form-control" name="sub_title"  placeholder="Subject Title">
                 </div>
                 <div class="form-group">
                     <label>Department:</label>
                     <input type="text" class="form-control" name="department"  placeholder="Department">
                 </div>
                 <button type="submit" class="btn btn-default btn-primary">Submit</button>
             </form>
         </fieldset>
      </div>
  </div>
</div>

<?php include 'footer.php'; ?>

